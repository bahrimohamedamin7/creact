<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\JWT;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Contracts\JWTSubject as JWTSubject;

class UserController extends Controller
{
    public function register(Request $request)
    {
    	$request['password'] = Hash::make($request->password);
    	$email = $request->email;
    	$Userdata = User::where('email', $email)->get();
    	if(count($Userdata) == 0)
    	{
            $data = User::create($request->all());
            $data->role = "0";
            $data->save();
    	    $response['message'] = 'User Registration Successfully';
    	    $response['data'] = $request->all();
    	    $response['status'] = 1;
    	}
        else
    	  {
    	  	$response['status'] = 0;
    	  	$response['message'] = 'Email already exists';
    	  }

        return response()->json($response);
    }

    public function login(Request $request)
    {
    	$email = $request->email;
    	$password = $request->password;
    	$userdata = User::where('email', $email)->first();
    	$result = Hash::check($password, $userdata->password);
    	if ($userdata && $result) //(($Userdata->email == $email) && $result)
    	{
    		$payload = [
                'name' => $userdata->name,
                'email' => $userdata->email,
                'password' => $userdata->password,
                'id' => $userdata->id,
            ];
            // $token = JWTAuth::attempt($payload);
            $token = JWTAuth::fromUser($userdata,$payload);
    	    $response['token'] = $token;
    	    $response['id'] = $payload['id'];
            // dd($response['id']);
    	    $response['status'] = 1;
    	    $response['message'] = 'Login Successfully';
    	}
    	else
    	{
    		$response['status'] = 0;
    	  	$response['message'] = 'Email or Password is wrong';
    	}

        return response()->json($response);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$id)
    {
        $user = User::find($id);
        $skip = $request->skip;
    	$limit = $request->limit;
        if ($user->role == "1")
        {
            $users = User::where('role','0')->skip($skip)->take($limit)->get();
            $total = User::count();
            $response["users"] = $users;
            $response["total"] = $total;
            return response()->json($response);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('file');
        $uploadPath = "images";
        $originalImage = $file->getClientOriginalName();
        $file->move($uploadPath, $originalImage);
        // dd(json_decode($request->data));
        $password = Hash::make(json_decode($request->data)->password);
        $userData = json_decode($request->data, true);
        // dd($userData);
        $password = Hash::make(json_decode($request->data)->password);
        $userData["image"] = $originalImage;
        // $userData = User::create($userData, $password, $request->all());
        $userData = User::create([
            'name'=> $userData["name"],
            'surname'=> $userData["surname"],
            'email'=> $userData["email"],
            'password'=> $password,
            'phone'=> $userData["phone"],
            'address'=> $userData["address"],
            'image'=> $originalImage,
            'role' => $userData["role"],

        ]);
        // dd($userData);
        return response()->json($userData);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return response()->json($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if (file_exists($user->image))
        {
            unlink('images/'.$user->image);
        }
    	$user->delete();
    	return response()->json($user);
    }

    public function backup()
    {
        // $link = scandir(storage_path('app/laravel'));
        // dd($link);
        \Artisan::call('backup:run');
        $response['msj'] = 1;
        $response['message'] = "Backup done successfully";
        // dd('ok');
    	return response()->json($response);
    }

    public function saveToken(Request $request)
    {
        $user = User::update(['device_token'=>$request->token]);
        return response()->json(['token saved successfully.']);
    }

    public function sendNotification(Request $request)
    {
        // dd($request->all());
        $data = array('title'=> $request->title,
                    'body' => $request->body);
        Mail::send('notifications', $data, function($message)
    	{
    		$message->subject('Notifications');
            $message->from('bahrimohamedamin7@gmail.com');
    		$message->to('bahrimohamedamin7@gmail.com');
    	});
        // dd('ok');
        $response = "Notification send to Adminstrator successfully";
        return response()->json($response);
        // $firebaseToken = User::whereNotNull('device_token')->pluck('device_token')->all();

        // $SERVER_API_KEY = 'AAAA0dMQoaE:APA91bGqAm5p3OTOFco-tTA5buNVek_RdnLgI1dd9togDV65dgYnjvykVPzq_uVXHsq-CcZt4eXnazEtF1XEm0737rzQRWhsc396jG_YSlXMqaWG7ZJOQ--f0ZNNXdGkm4r9vyys8YhO';

        // $data = [
        //     "registration_ids" => $firebaseToken,
        //     "notification" => [
        //         "title" => $request->title,
        //         "body" => $request->body,
        //     ]
        // ];
        // $dataString = json_encode($data);


        // $headers = [
        //     'Authorization: key=' . $SERVER_API_KEY,
        //     'Content-Type: application/json',
        // ];

        // // dd($headers);
        // $ch = curl_init();

        // curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        // curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

        // $response = curl_exec($ch);

        // dd($response);
    	// return response()->json($response);
    }
}
