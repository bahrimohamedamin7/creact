// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyBq78TH0BElyuBI1hFlLndKKynbTfSjroQ",
    authDomain: "calm-metric-272319.firebaseapp.com",
    projectId: "calm-metric-272319",
    storageBucket: "calm-metric-272319.appspot.com",
    messagingSenderId: "901189247393",
    appId: "1:901189247393:web:ec7e723a0f23f116b06be0",
    measurementId: "G-YQN6971TXN"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);