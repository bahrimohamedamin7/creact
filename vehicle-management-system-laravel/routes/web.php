<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\VehicleController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::resource('vehicules',VehicleController::class);
//vehicules
Route::post('vehicules/{id}',[VehicleController::class, 'index'])->middleware('verifuser');
Route::post('add_vehicule/{id}',[VehicleController::class, 'store'])->middleware('verifuser');
Route::delete('delete_vehicule/{id}',[VehicleController::class, 'destroy'])->middleware('verifuser');
Route::get('/get_vehicule_details/{id}', [VehicleController::class, 'show'])->middleware('verifuser');
Route::patch('/edit_vehicule_details/{id}', [VehicleController::class, 'update'])->middleware('verifuser');
//users
Route::post('users/{id}',[UserController::class, 'index'])->middleware('verifuser');
Route::post('add_user',[UserController::class, 'store']);
Route::delete('delete_user/{id}',[UserController::class, 'destroy']);
Route::get('getuserdetail/{id}',[UserController::class, 'show'])->middleware('verifuser');
Route::post('save-token', [UserController::class, 'saveToken'])->middleware('verifuser');
Route::post('notifications', [UserController::class, 'sendNotification'])->middleware('verifuser');
Route::post('login',[UserController::class, 'login']);
Route::post('register',[UserController::class, 'register']);
//backup
// Route::get('backup', function () {
//     Artisan::call('backup:run');
//     dd('ok');
//     return 'backup done successfully';
// })->middleware('verifuser');
Route::get('backup', [UserController::class, 'backup'])->middleware('verifuser');
