<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$id)
    {
        $skip = $request->skip;
    	$limit = $request->limit;
        $user = User::find($id);
        if($user->role == "0")
        {
            $vehicules = Vehicle::where('user_id',$id)->skip($skip)->take($limit)->get();
        }
        else
        {
            $vehicules = Vehicle::skip($skip)->take($limit)->get();
        }
    	$total = Vehicle::count();
    	$response["vehicules"] = $vehicules;
    	$response["total"] = $total;
    	return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $vehicule = Vehicle::create($request->all());
        $vehicule->user_id = $id;
        $vehicule->save();
        return response()->json($vehicule);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $id = $request->id;
    	$vehicule = Vehicle::find($id);
    	return response()->json($vehicule);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = $request->id;
    	$vehicule = Vehicle::findOrFail($id);
    	$vehicule->update($request->all());
    	return response()->json($vehicule);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $id = $request->id;
        // dd($id);
    	$vehicule = Vehicle::findOrFail($id);
    	$vehicule->delete();
    	return response()->json($vehicule);
    }
}
